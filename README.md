<br><br>

<h3 align="center">v-selectpage</h3>

<br>

<p align="center">
  <a href="https://www.npmjs.com/package/v-selectpage"><img src="https://img.shields.io/npm/v/v-selectpage.svg"></a>
  <a href="https://mit-license.org/"><img src="https://img.shields.io/badge/license-MIT-brightgreen.svg"></a>
  <a href="https://www.npmjs.com/package/v-selectpage"><img src="https://img.shields.io/npm/dy/v-selectpage.svg"></a>
</p>

<p align="center"><img src="https://terryz.gitee.io/image/v-selectpage/v-selectpage-multiple.png" alt="v-selectpage" ></p>

<p align="center">
基于 <strong>Vue2</strong> 强大的选择器, 可分页的列表或表格展现形式, <br>
使用标签形式的多选模式, 国际化 i18n 和服务端数据源支持
</p>

<p align="center">
  <a href="https://nodei.co/npm/v-selectpage/"><img src="https://nodei.co/npm/v-selectpage.png"></a>
</p>

<br><br><br><br><br>

## 文档、Demo
请浏览

- [English site](https://terryz.github.io/vue/#/selectpage)
- [国内站点](https://terryz.gitee.io/vue/#/selectpage)

jQuery 版本: [SelectPage](https://gitee.com/TerryZ/SelectPage)

**If you think this project is helpful, please star it.**

<br><br>

## 功能特点

- 分页展示数据
- i18n 支持, 提供了 中文、英文、日文 等语言
- 服务端数据源支持
- 使用标签模式进行项目多选
- 可使用键盘进行快速导航
- 提供快速搜索，进行快速数据定位
- 提供列表视图和表格视图进行展示数据
- 可自定义行显示方式


<br><br>

## Vue plugin series

| Plugin | Status | Description |
| :---------------- | :-- | :-- |
| [v-page](https://github.com/TerryZ/v-page) | [![npm version](https://img.shields.io/npm/v/v-page.svg)](https://www.npmjs.com/package/v-page) | A simple pagination bar, including length Menu, i18n support |
| [v-dialogs](https://github.com/TerryZ/v-dialogs) | [![npm version](https://img.shields.io/npm/v/v-dialogs.svg)](https://www.npmjs.com/package/v-dialogs) | A simple and powerful dialog, including Modal, Alert, Mask and Toast modes |
| [v-tablegrid](https://github.com/TerryZ/v-tablegrid) | [![npm version](https://img.shields.io/npm/v/v-tablegrid.svg)](https://www.npmjs.com/package/v-tablegrid) | A simpler to use and practical datatable |
| [v-uploader](https://github.com/TerryZ/v-uploader) | [![npm version](https://img.shields.io/npm/v/v-uploader.svg)](https://www.npmjs.com/package/v-uploader) | A Vue2 plugin to make files upload simple and easier, <br>you can drag files or select file in dialog to upload |
| [v-ztree](https://github.com/TerryZ/v-ztree) | [![npm version](https://img.shields.io/npm/v/v-ztree.svg)](https://www.npmjs.com/package/v-ztree) | A simple tree for Vue2, support single or multiple(check) select tree, <br>and support server side data |
| [v-gallery](https://github.com/TerryZ/v-gallery) | [![npm version](https://img.shields.io/npm/v/v-gallery.svg)](https://www.npmjs.com/package/v-gallery) | A Vue2 plugin make browsing images in gallery |
| [v-region](https://github.com/TerryZ/v-region) | [![npm version](https://img.shields.io/npm/v/v-region.svg)](https://www.npmjs.com/package/v-region) | A simple region selector, provide Chinese administrative division data |
| [v-selectpage](https://github.com/TerryZ/v-selectpage) | [![npm version](https://img.shields.io/npm/v/v-selectpage.svg)](https://www.npmjs.com/package/v-selectpage) | A powerful selector for Vue2, list or table view of pagination, <br>use tags for multiple selection, i18n and server side resources supports |

<br><br>

## 插件预览

*列表视图单选模式*

![single](https://terryz.gitee.io/image/v-selectpage/v-selectpage-single.png)

*列表视图使用标签的多选模式*

![multiple](https://terryz.gitee.io/image/v-selectpage/v-selectpage-multiple.png)

*表格视图*

![table](https://terryz.gitee.io/image/v-selectpage/v-selectpage-table.png)

<br><br>

## 安装

``` bash
npm install v-selectpage --save
```

在项目入口 `main.js` 中引入

```js
import Vue from 'vue'
import vSelectPage from 'v-selectpage';
Vue.use(vSelectPage);
```

## 应用在页面上

template

```html
<template>
    <v-selectpage :data="list" key-field="id" show-field="name" class="form-control"></v-selectpage>
</template>
```

script

```js
export default {
    data(){
        return {
            list: [
                {id:1 ,name:'Chicago Bulls',desc:'芝加哥公牛'},
                {id:2 ,name:'Cleveland Cavaliers',desc:'克里夫兰骑士'},
                {...}
            ]
        }
    }
};
```
<br><br>
